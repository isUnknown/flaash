panel.plugin("adrienpayet/callout-block", {
  blocks: {
    callout: {
      computed: {
        placeholder() {
          return "Button text …";
        },
      },
      template: `
        <textarea
          :placeholder="placeholder"
          @input="update({ text: $event.target.value })"
        >{{ content.text }}</textarea>
      `,
    },
  },
});
