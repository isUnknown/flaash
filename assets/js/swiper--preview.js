document.addEventListener("DOMContentLoaded", () => {
  const slidesPerView = window.innerWidth > 800 ? 3 : 1.15;

  const swiperPreview = new Swiper("#swiper--preview", {
    direction: "horizontal",
    slidesPerView,
    spaceBetween: 20,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    keyboard: true,
    loop: true,
  });
});
