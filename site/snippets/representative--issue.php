<div class="representative representative--issue">
    <a href="<?= $issue->url() ?>" title="Voir le numéro">
        <?php snippet('picture', ['file' => $issue->portraitCover()->toFile()]) ?>
        <?php if ($issue->hoverCover()->isNotEmpty()): ?>
            <?php snippet('picture', ['file' => $issue->hoverCover()->toFile()]) ?>
        <?php endif ?>
    </a>

    <h4 class="representative representative--issue__title">
        <a class="no-line" href=" <?= $issue->url() ?>" title="Voir le numéro">
            <?= $issue->numberedTitle() ?>
        </a>
    </h4>
    <p class="period period--small"><?= $issue->period() ?></p>
    <?php snippet('thematics', ['thematics' => $issue->thematics()->split()]) ?>
    
</div>