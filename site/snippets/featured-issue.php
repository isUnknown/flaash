<div class="featured-issue" style="--span:9">
  <a href="<?= $issue->url() ?>" title="Voir la page du dernier numéro.">
  <?php snippet('picture', ['file' => $issue->landscapeCover()->toFile()]) ?>
  </a>

  <div class="featured-issue__details">
    <div class="featured-isssue__left-columns">
      <?php snippet('thematics', ['thematics' => $issue->thematics()->split()]) ?>
      <a class="no-line" title="Aller au n°<?= $issue->number() ?>" href="<?= $issue->url() ?>"><h3><?= $issue->numberedTitle() ?></h3></a>
      <p class="period period--small"><?= $issue->period() ?></p>
      <div class="featured-issue__presentation">
        <?= $issue->presentation() ?>
      </div>
    </div>
    <div class="featured-isssue__right-columns">
      <?php snippet('thematics', ['thematics' => $issue->thematics()->split()]) ?>
      <div class="featured-issue__buttons">
        <?php if ($issue->orderLink()->isNotEmpty()): ?>
          <a href="<?= $issue->orderLink() ?>" class="button" target="_blank">Commander</a>
        <?php endif ?>
        <a href="<?= page('boutique')->printSubmissionLink() ?>" class="button" target="_blank">S'abonner</a>
      </div>
    </div>
  </div>
</div>