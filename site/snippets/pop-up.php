<div class="pop-up-overlay hidden" title="fermer la pop-up"></div>
<div class="pop-up-wrapper hidden">
  <div class="pop-up">
    <button class="close-pop-up" title="fermer la pop-up">
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" fill="currentColor"><path d="M10.5859 12L2.79297 4.20706L4.20718 2.79285L12.0001 10.5857L19.793 2.79285L21.2072 4.20706L13.4143 12L21.2072 19.7928L19.793 21.2071L12.0001 13.4142L4.20718 21.2071L2.79297 19.7928L10.5859 12Z"></path></svg>
    </button>
    <div class="left-column">
      <h3><?= $site->popUpText() ?></h3>
      <form action="submit">
        <input class="invert" placeholder="Email" type="email" name="email" autocomplete="email" id="email">
        <button type="submit">S'inscrire</button>
      </form>
    </div>
    <?php if ($site->popUpImage()->isNotEmpty()): ?>
      <?php
        $file = $site->popUpImage()->toFile();
      ?>
      <picture>
        
        <source srcset="<?= $file->srcset('webp') ?>"
            sizes="(min-width: 1200px) 25vw, (min-width: 900px) 33vw, (min-width: 600px) 50vw, 100vw" type="image/webp">
        <img 
            src="<?= $file->url() ?>"
            srcset="<?= $file->srcset('webp') ?>"
            sizes='(min-width: 1200px) 25vw, (min-width: 900px) 33vw, (min-width: 600px) 50vw, 100vw'
            width="<?= $file->resize(800)->width() ?>"
            height="<?= $file->resize(800)->height() ?>"
            alt="<?= $file->alt() ?>"
            loading="lazy"
        >
        <?= svg('assets/images/loader.svg') ?>
    </picture>
    <?php endif ?>
  </div>
</div>