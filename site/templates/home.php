<?php snippet('header') ?>

<?php
  $currentIssue = page('numeros')->children()->first();
  $featuredArticle  = page('articles')->featuredArticle()->isNotEmpty() ? page('articles')->featuredArticle()->toPage() : $kirby->collection('articles')->first();
?>

<section id="hero" class="full">
  <?php if ($page->heroLink()->isNotEmpty()): ?>
    <a href="<?= $page->heroLink() ?>"<?= e(!str_contains($page->heroLink(), $site->url()), ' target="_blank"') ?>>
      <?php snippet('picture', ['file' => $page->cover()->toFile(), "crop" => "banner"]) ?>
    </a>
  <?php else: ?>
    <?php snippet('picture', ['file' => $page->cover()->toFile(), "crop" => "banner"]) ?>
  <?php endif ?>

  <?php snippet('card', [
      'title'   => $page->featuredCardTitle(),
      'image'   => $currentIssue->portraitCover()->toFile(),
      'link' => $currentIssue->url(),
      'buttons' => [
          [
            'text' => $page->featuredCardButton(),
            'link' => $currentIssue->orderLink()
          ]
      ]
  ]) ?>
</section>
<section id="about">
  <?= $page->about() ?>
</section>

<section id="current-issue">
  <a href="<?= $currentIssue->url() ?>" class="no-line"><h3 class="section-title--left">Numéro en cours</h3></a>
  <div class="grid">
    <?php snippet('featured-issue', ['issue' => $currentIssue]) ?>
    <div style="--span:3">
      <?php snippet('card', [
          'title'   => 'Nous trouver',
          'image' => page('nous-trouver')->cardCover()->toFile() ?? null,
          'link' => 'https://web2store.mlp.fr/produit.aspx?edi_code=lT4Hrvj3Q%2bc%3d&tit_code=iTK83lfTxcA%3d',
          'buttons' => [
              [
                  'text' => 'Point presse<br />le plus proche',
                  'link' => 'https://web2store.mlp.fr/produit.aspx?edi_code=lT4Hrvj3Q%2bc%3d&tit_code=iTK83lfTxcA%3d'
              ]
          ]
      ]) ?>
    </div>
  </div>
</section>

<section id="articles">
  <a href="<?= page('articles')->url() ?>" class="section-title--center"><h3>Articles</h3></a>
  <div class="grid">
    <?php snippet('featured-article', ['article' => $featuredArticle]) ?>
    
    <div class="grid" id="articles__list--mobile">
      <?php foreach($kirby->collection('articles')->without($featuredArticle)->limit(4) as $article): ?>
      <div style="--span: 3;">
        <?php snippet('representative--article', ['article' => $article]) ?>
      </div>
      <?php endforeach ?>
    </div>
    
    <div style="--span:3">
      <?php snippet('card', [
          'title'   => "S'abonner",
          'image' => page('boutique')->cardCover()->toFile() ?? null,
          'link' => page('boutique')->link(),
          'target' => 'external',
          'buttons' => [
              [
                  'text' => 'Abonnement papier',
                  'link' => page('boutique')->printSubmissionLink()
              ],
              [
                  'text' => 'Abonnement digital',
                  'link' => page('boutique')->digitalSubmissionLink()
              ]
          ]
      ]) ?>
    </div>
  </div>
  <a href="<?= page('articles')->url() ?>" id="other-articles-title" class="no-line section-title--left"><h3>Autres articles</h3></a>
  <div class="grid" id="articles__list">
    <?php foreach($kirby->collection('articles')->without($featuredArticle)->limit(4) as $article): ?>
    <div style="--span: 3;">
      <?php snippet('representative--article', ['article' => $article]) ?>
    </div>
    <?php endforeach ?>
  </div>
</section>

<?php snippet('shop') ?>

<section id="preview">
  <h3 class="section-title--center">Aperçus</h3>

  <div class="swiper" id="swiper--preview">
    <div class="swiper-wrapper">
      <?php foreach($page->gallery()->toFiles() as $slide): ?>
      <div class="swiper-slide">
        <?php snippet('picture', ['file' => $slide]) ?>
      </div>
      <?php endforeach ?>
    </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
  </div>

</section>
<?php snippet('lightbox', ['files' => $page->gallery()->toFiles()]) ?>
<script src="<?= url('assets/js/swiper--preview.js') ?>"></script>
<script src="<?= url('assets/dist/swiper--lightbox.min.js') ?>"></script>
<?php snippet('footer') ?>