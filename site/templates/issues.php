<?php snippet('header') ?>
<section class="grid">
  <div class="repository-presentation" style="--span: 7;">
    <h1><?= $page->title() ?></h1>
    <?= $page->presentation() ?>
  </div>
</section>
<section class="grid">
  <?php foreach($page->children() as $issue): ?>
    <div style="--span: 3;">
    <?php snippet('representative--issue', ['issue' => $issue]) ?>
    </div>
  <?php endforeach ?>
</section>
<?php snippet('shop') ?>
<?php snippet('footer') ?>