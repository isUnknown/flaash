function scrollToTop() {
  window.scrollTo({
    top: 0,
  });
}

document.addEventListener("DOMContentLoaded", () => {
  const timeout = navigator.userAgent.includes("Safari") ? 50 : 0;
  setTimeout(() => {
    // =========================================== LOGO STRETCHING
    const logo = document.querySelector("#main-logo");
    const monogram = document.querySelector("#monogram");
    const header = document.querySelector("#main-header");
    const initialWidth =
      logo.getBoundingClientRect().width / (window.innerWidth / 100);
    let dynamicWidth = initialWidth;
    const scrollThreshold = 100;
    const notes = document.querySelectorAll("article .note");
    let windowWidth = window.innerWidth;
    const submitBtns = document.querySelectorAll('button[type="submit"]');
    const mobileNavBtn = document.querySelector("#mobile-nav-btn");
    const mobileNav = document.querySelector("#main-nav--mobile");
    const popUp = {
      overlay: document.querySelector(".pop-up-overlay"),
      wrapper: document.querySelector(".pop-up-wrapper"),
      close: document.querySelector(".close-pop-up"),
      isVisible: localStorage.hasOwnProperty("popUp") ? false : true,
    };

    function stretchLogo() {
      const minWidth = 200 / (window.innerWidth / 100);

      const isOverThreshold = window.scrollY > scrollThreshold;

      if (isOverThreshold) {
        header.classList.add("expanded");

        dynamicWidth = initialWidth - (window.scrollY - scrollThreshold) / 20;
      } else {
        header.classList.remove("expanded");
        dynamicWidth = initialWidth;
      }

      logo.style.width =
        dynamicWidth > minWidth ? dynamicWidth + "vw" : minWidth + "vw";
    }

    function toggleNote(event) {
      const note = event.target;
      if (note.textContent === "[note]") {
        note.innerHTML = `[${note.dataset.note}]`;
        note.setAttribute("title", "Fermer la note");
      } else {
        note.innerHTML = "[note]";
        note.setAttribute("title", "Ouvrir la note");
      }
    }

    function rotateMonogram() {
      monogram.style.transform = `rotate(${window.scrollY / 3}deg)`;
    }

    function submitToNewsletter(email, btn) {
      btn.textContent = "En cours…";
      const init = {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email: email }),
      };

      fetch("/submit.json", init)
        .then((response) => response.json())
        .then((json) => {
          btn.textContent = "Merci !";
          btn.classList.add("success");
          console.log(json);
        })
        .catch((error) => {
          btn.textContent = "Erreur";
          btn.classList.add("success");
          console.error(error);
        });
    }

    function toggleMobileNav() {
      mobileNavBtn.classList.toggle("open");
      mobileNav.classList.toggle("open");
      document.body.classList.toggle("no-scroll");
      header.classList.toggle("open");
    }

    function show(image) {
      image.classList.add("show");
      if ((svg = image.nextElementSibling)) {
        svg.classList.add("hide");
        image.parentNode.classList.add("hide");
      }
    }

    function hidePopUp() {
      if (!popUp.overlay) return;
      popUp.overlay.classList.add("hidden");
      popUp.wrapper.classList.add("hidden");
      document.body.classList.remove("no-scroll");
    }

    function showPopUp() {
      if (!popUp.overlay) return;
      popUp.overlay.classList.remove("hidden");
      popUp.wrapper.classList.remove("hidden");
      document.body.classList.add("no-scroll");
    }

    submitBtns.forEach((btn) => {
      btn.addEventListener("click", function (event) {
        event.preventDefault();
        const emailInput = btn.parentNode.querySelector('input[type="email"]');
        const email = emailInput.value;
        submitToNewsletter(email, btn);
      });
    });

    if (popUp.overlay && popUp.isVisible) {
      setTimeout(() => {
        showPopUp();
      }, 3000);
    }

    if (popUp.overlay) {
      popUp.overlay.addEventListener("click", () => {
        console.log("test");
        hidePopUp();
        localStorage.setItem("popUp", "viewed");
      });
      popUp.close.addEventListener("click", () => {
        hidePopUp();
        localStorage.setItem("popUp", "viewed");
      });
    }

    window.addEventListener("scroll", (event) => {
      if (window.scrollY > 100) {
        monogram.classList.add("show");
      } else {
        monogram.classList.remove("show");
      }
      if (window.innerWidth > 800) {
        stretchLogo();
      } else {
        if (window.scrollY > scrollThreshold) {
          header.classList.add("expanded");
        } else {
          header.classList.remove("expanded");
        }
      }
      rotateMonogram();
    });

    notes.forEach((note) => {
      note.addEventListener("click", toggleNote);
    });

    mobileNavBtn.addEventListener("click", () => {
      toggleMobileNav();
    });

    const images = document.querySelectorAll("img");
    images.forEach((image) => {
      if (image.complete) {
        show(image);
      } else {
        image.addEventListener("load", () => {
          show(image);
        });
      }
    });
  }, timeout);
});
