</main>
<a id="monogram" title="Aller à l'accueil" onclick="scrollToTop()">
  <?= svg('assets/images/logos/monogram.svg') ?>
</a>
<footer id="main-footer">

  <div id="main-footer__logo" class="main-footer__column">
    <?php if ($logo = asset('assets/images/logos/logo-caption-white.svg')): ?>
    <img src="<?= $logo->url() ?>"
      alt="FLAASH - La revue culturelle et technique d'anticipation.">
    <?php endif ?>
    <p>©Flaash — <?= date('Y') ?></p>
  </div>

  <div id="main-footer__mail" class="main-footer__column">
    <p>Adresse mail</p>
    <a href="mailto:contact@flaash.fr" title="Écrire à la revue">contact@flaash.fr</a>
  </div>

  <div id="main-footer__social-media" class="main-footer__column">
    <p>Réseaux sociaux</p>
    <ul>
      <li class="social-icon">
        <a href="<?= $site->instagram() ?>" target="_blank" title="Aller à la page Instagram">
          <?= svg('assets/images/icons/instagram.svg') ?>
        </a>
      </li>
      <li class="social-icon">
        <a href="<?= $site->linkedin() ?>" target="_blank" title="Aller à la page LinkedIn">
          <?= svg('assets/images/icons/linkedin.svg') ?>
        </a>
      </li>
    </ul>
  </div>

  <div id="main-footer__pages" class="main-footer__column">
    <nav>
      <ul>
        <?php foreach($site->footerNav()->toPages() as $footerNavPage): ?>
            <li>
              <a 
                class="regular" 
                href="<?= $footerNavPage->url() ?>"
                <?= e(!str_contains($footerNavPage->url(), $site->url()), 'target="_blank"') ?>
              ><?= $footerNavPage->title() ?></a>
            </li>
        <?php endforeach ?>
      </ul>
    </nav>
  </div>

  <div id="main-footer__newsletter" class="main-footer__column">
    <p>Newsletter</p>
    <form action="">
      <input class="invert" placeholder="Email" type="email" name="email" autocomplete="email" id="email">
      <button class="invert" type="submit">S'inscrire</button>
    </form>
  </div>
</footer>
<footer id="main-footer--mobile">

  <div id="main-footer--mobile__logo">
    <?php if ($logo = asset('assets/images/logos/logo-caption-white.svg')): ?>
    <img src="<?= $logo->url() ?>"
      alt="FLAASH - La revue culturelle et technique d'anticipation.">
    <?php endif ?>
    <p>©Flaash — <?= date('Y') ?></p>
  </div>

  <div id="main-footer--mobile__pages">
    <nav>
      <ul>
        <?php foreach($site->footerNav()->toPages() as $footerNavPage): ?>
            <li>
              <a 
                class="regular" 
                href="<?= $footerNavPage->url() ?>"
                <?= e(!str_contains($footerNavPage->url(), $site->url()), 'target="_blank"') ?>
              ><?= $footerNavPage->title() ?></a>
            </li>
        <?php endforeach ?>
      </ul>
    </nav>
  </div>

  <div id="main-footer--mobile__mail-socials">
    <a href="mailto:contact@flaash.fr" title="Écrire à la revue">contact@flaash.fr</a>
    <ul>
      <li class="social-icon">
        <a href="<?= $site->instagram() ?>" target="_blank" title="Aller à la page Instagram">
          <?= svg('assets/images/icons/instagram.svg') ?>
        </a>
      </li>
      <li class="social-icon">
        <a href="<?= $site->linkedin() ?>" target="_blank" title="Aller à la page LinkedIn">
          <?= svg('assets/images/icons/linkedin.svg') ?>
        </a>
      </li>
    </ul>
  </div>

  <div id="main-footer--mobile__newsletter" >
    <p>Newsletter</p>
    <form action="">
      <input class="invert" placeholder="Email" type="email" name="email" autocomplete="email" id="email">
      <button class="invert" type="submit">S'inscrire</button>
    </form>
  </div>
</footer>
<?php snippet('edit-button', ['text' => 'Éditer la page']) ?>
<?php snippet('seo/schemas'); ?>
</body>

</html>