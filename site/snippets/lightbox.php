<div id="lightbox" class="hidden">
  <button id="lightbox__close-btn" title="Fermer"><?= svg('assets/images/icons/close.svg') ?></button>
  <div class="swiper" id="swiper--lightbox" style="--span: 8;">
    <div class="swiper-wrapper">
      <?php foreach($files as $slide): ?>
      <div class="swiper-slide" title="Agrandir">
        <?php snippet('picture', ['file' => $slide]) ?>
      </div>
      <?php endforeach ?>
    </div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
  </div>
</div>