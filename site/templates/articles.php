<?php 
$articles = $kirby->collection('articles')
?>
<?php snippet('header') ?>
<section class="grid">
  <div class="repository-presentation" style="--span: 7;">
    <h1><?= $page->title() ?></h1>
    <?= $page->presentation() ?>
  </div>
</section>
<section class="grid">
  <?php foreach($articles as $article): ?>
    <div style="--span: 3;">
    <?php snippet('representative--article', ['article' => $article]) ?>
    </div>
  <?php endforeach ?>
</section>

<section id="current-issue">
  <a href="<?= page('numeros')->children()->first()->url() ?>" class="no-line"><h3 class="section-title--left">Numéro en cours</h3></a>
  <div class="grid">
    <?php snippet('featured-issue', ['issue' => page('numeros')->children()->first()]) ?>
    <div style="--span:3">
      <?php snippet('card', [
          'title'   => 'Nous trouver',
          'image' => page('nous-trouver')->cardCover()->toFile() ?? null,
          'link' => 'https://web2store.mlp.fr/produit.aspx?edi_code=lT4Hrvj3Q%2bc%3d&tit_code=iTK83lfTxcA%3d',
          'buttons' => [
              [
                  'text' => 'Point presse<br />le plus proche',
                  'link' => 'https://web2store.mlp.fr/produit.aspx?edi_code=lT4Hrvj3Q%2bc%3d&tit_code=iTK83lfTxcA%3d'
              ]
          ]
      ]) ?>
    </div>
  </div>
</section>
<?php snippet('footer') ?>