document.addEventListener("DOMContentLoaded", () => {
  const swiperIssue = new Swiper("#swiper--issue", {
    direction: "horizontal",
    slidesPerView: 1,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    keyboard: true,
    loop: true,
    pagination: {
      el: ".swiper-pagination",
    },
  });
});
