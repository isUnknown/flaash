<?php snippet('header') ?>
<?php snippet('lightbox', ['files' => $page->gallery()->toFiles()]) ?>
<section id="issue" class="grid">
  <div class="swiper" id="swiper--issue" style="--span: 8;">
    <div class="swiper-wrapper">
      <?php foreach($page->gallery()->toFiles() as $slide): ?>
      <div class="swiper-slide">
        <?php snippet('picture', ['file' => $slide]) ?>
      </div>
      <?php endforeach ?>
    </div>
    
    <div class="swiper-pagination"></div>

    <div class="swiper-button-prev"></div>
    <div class="swiper-button-next"></div>
  </div>
  <div id="issue-description" style="--span: 4;">
    <h1><?= $page->numberedTitle() ?></h1>
    <p id="issue__period"><?= $page->period() ?></p>
    <?php snippet('thematics', ['thematics' => $page->thematics()->split()]) ?>
    <h3 id="issue__price">Prix : <?= $page->price() ?> €</h3>
    <?php if ($page->orderLink()->isNotEmpty()): ?>
      <a href="<?= $page->orderLink() ?>" class="button" target="_blank">Commander</a>
    <?php endif ?>
    <a href="<?= page('boutique')->printSubmissionLink() ?>" class="button" target="_blank">S'abonner</a>
    <?= $page->presentation() ?>
  </div>
</section>

<?php if ($page->hasChildren()): ?>  
  <section>
    <h3>À lire dans ce numéro</h3>
    <div class="grid">
      <?php snippet('featured-article', ['article' => $page->children()->first()]) ?>
      <div style="--span:3">
      <?php snippet('card', [
            'title'   => "S'abonner",
            'image' => page('boutique')->cardCover()->toFile() ?? null,
            'link' => page('boutique')->url(),
            'buttons' => [
                [
                    'text' => 'Abonnement papier',
                    'link' => page('boutique')->printSubmissionLink()
                ],
                [
                    'text' => 'Abonnement digital',
                    'link' => page('boutique')->digitalSubmissionLink()
                ]
            ]
        ]) ?>
      </div>
    </div>
  </section>
<?php endif ?>

<section>
  <h3>Autres articles à découvrir</h3>
  <div class="grid">
    <?php foreach($kirby->collection('articles')->limit(4)->shuffle() as $article): ?>
    <div style="--span: 3;">
      <?php snippet('representative--article', ['article' => $article]) ?>
    </div>
    <?php endforeach ?>
  </div>
</section>
<script src="<?= url('assets/dist/swiper--issue.min.js') ?>"></script>
<script src="<?= url('assets/dist/swiper--lightbox.min.js') ?>"></script>
<?php snippet('footer') ?>