<?php
  $lastName = count(explode(' ', $contributorField->toPage()->title()->value())) > 1 ? explode(' ', $contributorField->toPage()->title()->value())[1] : $contributorField->toPage()->title()->value();
  $initial = substr($lastName, 0, 1);
  $lowerInitial = strtolower($initial);
?>

<a
  href="<?= page('contributeurs')->url() ?>#letter-<?=$lowerInitial ?>" title="Voir la liste des contributeurs"><?= $contributorField->toPage()->title() ?></a>