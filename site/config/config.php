<?php

return [
    'debug'  => true,
    'api' => [
        'basicAuth' => true,
        'allowInsecure' => true
    ],
    'thumbs' => [
        'quality' => 80,
        'presets' => [
            'default' => [
                'width' => 1024, 'format' => 'webp'
            ],
            'full' => 2048, 'format' => 'webp'
        ],
        'srcsets' => [
            'default' => [
                '200w'  => ['width' => 400],
                '400w'  => ['width' => 800],
                '800w'  => ['width' => 3200],
                '2048w' => ['width' => 4096]
            ],
            'webp' => [
                '200w'  => ['width' => 400, 'format' => 'webp'],
                '400w'  => ['width' => 800, 'format' => 'webp'],
                '800w'  => ['width' => 1600, 'format' => 'webp'],
                '2048w' => ['width' => 4096, 'format' => 'webp']
            ]
        ],
    ],
    'panel' => [
        'css' => 'assets/css/custom-panel.css',
        'menu' => require __DIR__ . '/menu.php',
    ],
    'routes' => [
      [
        'pattern' => '/submit.json',
        'method' => 'POST',
        'action' => function() {            
            $json = file_get_contents('php://input');
            $data = json_decode($json);
            
            try {
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => 'https://us21.api.mailchimp.com/3.0/lists/d2561d8495/members',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS =>json_encode([
                    'email_address' => $data->email,
                    'status' => 'subscribed'
                ]),
                CURLOPT_HTTPHEADER => array(
                    'Content-Type: application/json',
                    'Authorization: apikey 80f4c0d28dab9328de975d20e8402094-us21'
                ),
                ));

                $response = curl_exec($curl);

                curl_close($curl);
                return [
                    'status' => 'success',
                    'response' => json_decode($response)
                ];
            } catch (\Throwable $th) {
                return [
                    'status' => 'error',
                    'message' => $th->getMessage()
                ];
            }
        }
      ]  
    ],
    'hooks' => [
        'page.changeStatus:after' => function ($newPage, $oldPage) {
            if ($newPage->template() == 'issue' && $newPage->status() != 'draft') {
                foreach ($newPage->childrenAndDrafts() as $article) {
                    if ($article->template() == 'article' && $article->contributor()->isEmpty()) {
                        throw new Exception("Impossible de publier l'article « " . $article->title()->value() . " ». Veuillez lui associer un contributeur puis le publier manuellement." , 1);
                    } else {
                        $article->changeStatus('listed');
                    }
                }
            }
        }
    ],
    'tobimori.seo.lang' => 'fr_FR'
];