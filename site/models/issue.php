<?php

class IssuePage extends Page {
    public function numberedTitle() {
        return 'n°' . $this->number() . ' — ' . $this->title();
    }
}