<?php

return function() {
    $getInitial = function($title) {
        $names = explode(' ', $title);
        $lastName = count($names) > 1 ? $names[1] : $names[0];
        return Str::substr($lastName, 0, 1);
    };

    $contributors = page('contributeurs')->children();

    $groups = $contributors->group(function($contributor) use ($getInitial) {
        return $getInitial($contributor->title()->value());
    });

    $sortedGroups = $groups->map(function($group) {
        return $group->sortBy(function($contributor) {
            $names = explode(' ', $contributor->title()->value());
            $lastName = count($names) > 1 ? $names[1] : $names[0];
            return $lastName;
        }, 'asc');
    });

    $sortedGroupsArray = $sortedGroups->toArray();
    ksort($sortedGroupsArray); 

    return $sortedGroupsArray;
};