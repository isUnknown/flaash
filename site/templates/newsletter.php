<?php snippet('header') ?>
<?php// snippet('picture', [
    // 'file' => $page->cover()->toFile(),
    // 'crop' => 'banner',
  // ]) ?>
<article>
  <header>
    <div id="article__infos">
      <h1><?= $page->title() ?></h1>
      <p>
        Publié le
        <?= convertDateToFrench($page->created()->toDate('d/m/Y')) ?>
        <br>
      </p>
      <?php snippet('thematics', ['thematics' => $page->thematics()->split()]) ?>
    </div>
    <div id="chapo">
      <?= $page->chapo() ?>
    </div>
  </header>

  <?= $page->body()->toBlocks() ?>

  <section id="other-newsletters">
    <h3>Autres newsletters</h3>
    <div class="grid">
      <?php foreach($page->parent()->children()->without($page->uri())->limit(4)->shuffle() as $newsletter): ?>
      <div style="--span: 3;">
        <?php snippet('representative--article', ['article' => $newsletter]) ?>
      </div>
      <?php endforeach ?>
    </div>
  </section>

</article>
<?php snippet('footer') ?>