<?php


return [
  'home' => [
    'label' => 'Accueil',
    'icon' => 'home',
    'link' => 'pages/home',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/home');
    }
  ],
  'site' => [
    'label' => 'Transverse',
    'icon' => 'shuffle',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'site');
    }
  ],
  '-',
  'issues' => [
    'label' => 'Numéros',
    'icon' => 'book',
    'link' => 'pages/numeros',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/numeros');
    }
  ],
  'articles' => [
    'label' => 'Articles',
    'icon' => 'document',
    'link' => 'pages/articles',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/articles');
    }
  ],
  '-',
  'contributors' => [
    'label' => 'Contributeurs',
    'icon' => 'edit',
    'link' => 'pages/contributeurs',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/contributeurs');
    }
  ],
  'thematics' => [
    'label' => 'Thématiques',
    'icon' => 'bookmark',
    'link' => 'pages/thematiques',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/thematiques');
    }
  ],
  '-',
  'shop' => [
    'label' => 'Boutique',
    'icon' => 'cart',
    'link' => 'pages/boutique',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/boutique');
    }
  ],
  'newsletter' => [
    'label' => 'Newsletter',
    'icon' => 'email',
    'link' => 'pages/newsletter',
    'current' => function ($current) {
        $path = Kirby::instance()->request()->path()->toString();
        return Str::contains($path, 'pages/newsletter');
    }
  ],
  'about' => [
    'label' => 'À propos',
    'icon' => 'info',
    'link' => 'pages/a-propos',
    'current' => function ($current) {
      $path = Kirby::instance()->request()->path()->toString();
      return Str::contains($path, 'pages/a-propos');
    }
  ],
  'find-us' => [
    'label' => 'Nous trouver',
    'icon' => 'pin',
    'link' => 'pages/nous-trouver',
    'current' => function ($current) {
      $path = Kirby::instance()->request()->path()->toString();
      return Str::contains($path, 'pages/nous-trouver');
    }
  ],
  'legal' => [
    'label' => 'Mentions légales',
    'icon' => 'info',
    'link' => 'pages/mentions-legales',
    'current' => function ($current) {
      $path = Kirby::instance()->request()->path()->toString();
      return Str::contains($path, 'pages/mentions-legales');
    }
  ],
  '-',
  '-',
  'comments',
  'users',
  'system'
];
