<?php snippet('header') ?>
<?php snippet('picture', [
  'file' => $page->cover()->toFile(),
  'crop' => 'banner'
]) ?>
<article>
  <header>
    <div id="article__infos">
      <h1><?= $page->title() ?></h1>
      <p>
        Publié le <?= convertDateToFrench($page->created()->toDate('d/m/Y')) ?>
        <br>
        <?php if ($page->contributor()->isNotEmpty()): ?>
          <span class="bold">écrit par</span> <?php snippet('contributor-link', ['contributorField' => $page->contributor()]) ?> <span class="bold">et <?= e($page->imagesType()->isNotEmpty() && $page->imagesType() == 'photos', 'photographié', 'illustré' ) ?> par</span> <?= $page->illustrator() ?>
        <?php endif ?>
      </p>
      <?php snippet('thematics', ['thematics' => $page->thematics()->split()]) ?>
    </div>
    <div id="chapo">
      <?= $page->chapo() ?>
    </div>
  </header>
  
  <?= $page->body()->toBlocks() ?>
</article>

<?php if ($page->parent()->template() == "issue"): ?>  
  <section>
    <a href="<?= $page->parent()->url() ?>" class="no-line"><h3 class="section-title--left">À retrouver dans ce numéro</h3></a>
    <div class="grid">
      <?php snippet('featured-issue', ['issue' => $page->parent()]) ?>
      <div style="--span:3">
        <?php snippet('card', [
            'title'   => 'Nous trouver',
            'link' => 'https://web2store.mlp.fr/produit.aspx?edi_code=lT4Hrvj3Q%2bc%3d&tit_code=iTK83lfTxcA%3d',
            'image' => page('nous-trouver')->cardCover()->toFile() ?? null,
            'buttons' => [
                [
                    'text' => 'Point presse<br />le plus proche',
                    'link' => 'https://web2store.mlp.fr/produit.aspx?edi_code=lT4Hrvj3Q%2bc%3d&tit_code=iTK83lfTxcA%3d'
                ]
            ]
        ]) ?>
      </div>
    </div>
  </section>
<?php endif ?>

<section>
  <h3>Autres articles à découvrir</h3>
  <div class="grid">
    <?php foreach($kirby->collection('articles')->limit(4)->shuffle() as $article): ?>
    <div style="--span: 3;">
      <?php snippet('representative--article', ['article' => $article]) ?>
    </div>
    <?php endforeach ?>
  </div>
</section>
<?php snippet('footer') ?>