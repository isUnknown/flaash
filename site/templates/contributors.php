<?php snippet('header') ?>
<?php
  $alphabet = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'];
  $activeLetters = array_keys($kirby->collection('contributors-groups'));
?>
<section class="grid">
  <div class="repository-presentation" style="--span: 7;">
    <h1><?= $page->title() ?></h1>
  </div>
</section>
<section id="anchors">
  <?php foreach($alphabet as $letter): ?>
    <?php if (in_array($letter, $activeLetters)): ?>
      <a href="#letter-<?= $letter ?>" class="no-line"><?= $letter ?></a>
    <?php else: ?>
      <p class="no-line disabled"><?= $letter ?></p>
    <?php endif ?>
    
  <?php endforeach ?>
</section>

<?php foreach($kirby->collection('contributors-groups') as $letter => $contributors): ?>
<section id="letter-<?= $letter ?>" class="letter">
    <div class="results__banner">
      <h2><?= $letter ?></h2>
    </div>
    <ul class="contributors-group grid">
      <?php foreach($contributors as $contributor): ?>
          <li class="contributor" style="--span: 3;">
              <h3 class="contributor__name"><?= $contributor->title() ?></h3>
              <div class="contributor__presentation"><?= $contributor->presentation() ?></div>
              <ul class="contributor__issues">
                <?php if ($contributor->issues()): ?>  
                  <?php foreach($contributor->issues() as $issue): ?>
                      <li class="contributor__issue">
                        <?php if (isset($issue['title'])): ?>
                          <a href="<?= $issue['url'] ?>"><h4 class="contributor__issue">n°<?=$issue['number'] ?> — <?= $issue['title'] ?></h4></a>    
                        <?php else: ?>
                          <a href="<?= $issue['url'] ?>"><h4 class="contributor__issue">Articles</h4></a>
                        <?php endif ?>
                        <ul>
                          <?php foreach($issue['articles'] as $article): ?>
                          <li class="contributor__article">
                            <p><?= $article->title() ?></p>
                          </li>
                          <?php endforeach ?>
                        </ul>
                      </li>
                  <?php endforeach ?>
                <?php endif ?>
              </ul>
          </li>
      <?php endforeach ?>
    </ul>
</section>
<?php endforeach ?>

<?php snippet('footer') ?>