const gulp = require("gulp");
const cssImport = require("gulp-cssimport");
const rename = require("gulp-rename");
const cleanCSS = require("gulp-clean-css");
const terser = require("gulp-terser");

const paths = {
  styles: {
    src: "assets/css/style.css",
  },
  scripts: {
    main: "assets/js/script.js",
    swiperIssue: "assets/js/swiper--issue.js",
    swiperLightbox: "assets/js/swiper--lightbox.js",
    swiperPreview: "assets/js/swiper--preview.js",
  },
  dest: "assets/dist/",
};

function packCss() {
  return gulp
    .src(paths.styles.src)
    .pipe(cssImport())
    .pipe(gulp.dest(paths.dest));
}

function minifyMainJS() {
  return gulp
    .src(paths.scripts.main)
    .pipe(rename("script.min.js"))
    .pipe(terser())
    .pipe(gulp.dest(paths.dest));
}
function minifySwiperIssueJS() {
  return gulp
    .src(paths.scripts.swiperIssue)
    .pipe(rename("swiper--issue.min.js"))
    .pipe(terser())
    .pipe(gulp.dest(paths.dest));
}
function minifySwiperLightboxJS() {
  return gulp
    .src(paths.scripts.swiperLightbox)
    .pipe(rename("swiper--lightbox.min.js"))
    .pipe(terser())
    .pipe(gulp.dest(paths.dest));
}
function minifySwiperPreviewJS() {
  return gulp
    .src(paths.scripts.swiperPreview)
    .pipe(rename("swiper--preview.min.js"))
    .pipe(terser())
    .pipe(gulp.dest(paths.dest));
}

exports.default = gulp.parallel(
  packCss,
  minifyMainJS,
  minifySwiperIssueJS,
  minifySwiperLightboxJS,
  minifySwiperPreviewJS
);
