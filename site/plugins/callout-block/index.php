<?php

Kirby::plugin('adrienpayet/callout-block', [
  'blueprints' => [
    'blocks/callout' => __DIR__ . '/blueprints/blocks/callout.yml'
  ],
  'snippets' => [
    'blocks/callout' => __DIR__ . '/snippets/blocks/callout.php'
  ]
]);