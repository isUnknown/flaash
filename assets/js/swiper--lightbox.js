function closeLightbox() {
  document.querySelector("#lightbox").classList.add("hidden");
}

document.addEventListener("DOMContentLoaded", () => {
  function openLightbox(event) {
    if (window.innerWidth <= 800) return;
    document.querySelector("#lightbox").classList.remove("hidden");

    if (event) {
      const slide = event.target.closest(".swiper-slide");
      const index = parseInt(slide.dataset.swiperSlideIndex);
      swiperLightbox.slideTo(index, 100);
    }
  }
  var swiperLightbox = new Swiper("#swiper--lightbox", {
    direction: "horizontal",
    slidesPerView: 1,
    navigation: {
      nextEl: ".swiper-button-next",
      prevEl: ".swiper-button-prev",
    },
    keyboard: true,
    loop: true,
  });

  document.addEventListener("keydown", (event) => {
    if (event.key === "Escape") {
      closeLightbox();
    }
  });

  document.querySelectorAll(".swiper-slide").forEach((slide) => {
    slide.addEventListener("click", openLightbox);
  });
  document
    .querySelector("#lightbox__close-btn")
    .addEventListener("click", closeLightbox);
});
