<?php
   
    $text = isset($text) ? $text : 'Edit';
    $url  = null;
    
    if (isset($home) && $home === 'site' && $page->isHomePage()) {
        $url = $site->panel()->url();
    } else {
        $url = $page->panel()->url();
    }
?>
<?php if ($kirby->user()): ?>
<div id="edit-button-wrapper">
    <a id="edit-button" title="Éditer la page"
        href="<?= $url ?>"><?= $text ?>
    </a>
</div>
<?php endif ?>