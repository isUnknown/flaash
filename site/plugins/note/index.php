<?php

Kirby::plugin('adrienpayet/plugin', [
    'tags' => [
        'note' => [
            'html' => function($tag) {
                return '<span class="note" data-note="' . $tag->value . '" title="Ouvrir la note">[note]</span>';
            }
        ]
    ]
]);