<div class="representative representative--article">
    <div class="representative--article__left">
        <?php if ($page->template() == 'articles' || $page->parent() == 'article'): ?>
            <?php if ($article->parent()->template() == "issue"): ?>
                <p class="period period--small"><a class="no-line" href="<?= $article->parent()->url() ?>" title="Voir le numéro">N°<?= $article->parent()->number() ?> — <?= $article->parent()->period() ?></a></p>
            <?php else: ?>
                <p class="period period--small"><?= page('articles')->unlistedLabel()->isNotEmpty() ? page('articles')->unlistedLabel() : "Hors-numéros" ?></p>
            <?php endif ?>
        <?php endif ?>
        <?php if ($page->template() == 'newsletter'): ?>
            <p class="period period--small"><?= convertDateToFrench($article->created()->toDate('d/m/Y')) ?></p>
        <?php endif ?>
    
        <a href="<?= $article->url() ?>" title="Voir l'article">
            <?php snippet('picture', ['file' => $article->cover()->toFile()]) ?>
        </a>
    </div>
    <div class="representative--article__right">
        <?php if ($page->template() == 'newsletter'): ?>
            <p class="period period--small mobile"><?= convertDateToFrench($article->created()->toDate('d/m/Y')) ?></p>
        <?php endif ?>
        <h4 class="representative representative--article__title">
            <a class="no-line" href=" <?= $article->url() ?>" title="Voir l'article">
                <?= $article->title() ?>
            </a>
        </h4>
        <?php if ($article->contributor()->exists()): ?>
            <p class="representative representative--article__writer">
            <?php snippet('contributor-link', ['contributorField' => $article->contributor()]) ?>
            </p>
        <?php endif ?>
        <?php snippet('thematics', ['thematics' => $article->thematics()->split()]) ?>
    </div>
</div>