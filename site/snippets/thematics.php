<ul class="thematics">
    <?php foreach($thematics as $thematic): ?>
    <li class="thematic">
        <a href="/thematiques?thematic=<?= urlencode($thematic) ?>"><?= $thematic ?></a>
    </li>
    <?php endforeach ?>
</ul>