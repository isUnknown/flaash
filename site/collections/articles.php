<?php

return function($site) {
    return $site->index()->filterBy('template', 'article')->sortBy('created', 'desc');
};