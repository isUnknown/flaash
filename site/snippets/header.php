<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>
        <?= e($page->isHomePage() != true, $page->title() . ' - ') . $site->title() ?>
    </title>

    <link rel="stylesheet" type="text/css"
        href="<?= url('assets/css/style.css?version-cache-prevent') . rand(0, 1000) ?>">

    <?php if ($page->template() == 'home' || $page->template() == 'issue'): ?>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.css" defer />
        <script src="https://cdn.jsdelivr.net/npm/swiper@11/swiper-bundle.min.js" defer></script>
    <?php endif ?>

    <?php if ($page->template() == 'thematics'): ?>
        <script defer src="https://cdn.jsdelivr.net/npm/alpinejs@3.x.x/dist/cdn.min.js" defer></script>
    <?php endif ?>

    <script src="<?= url('assets/js/script.js') ?>" defer>
    </script>
    
    <?php snippet('seo/head'); ?>
    <meta name="robots" content="noindex,nofollow">
    <?php snippet('front-comments', [
        'position' => 'bottom-left',
        'location' => 'assets'
    ]) ?>
    <style>
        :root {
            --color-accent-bg: <?= page('numeros')->children()->first()->color() ?>;
        }
    </style>

    <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
</head>

<body data-template="<?= $page->template() ?>">
    <?php if ($page->isHomePage() && $page->banner()->isNotEmpty()): ?>
        <div id="main-info">
            <p id="home-banner"><?= $page->banner()->inline() ?></p>
        </div>
    <?php endif ?>
    
    <?php if ($site->showPopUp() == "true"): ?>
        <?php snippet('pop-up') ?>
    <?php endif ?>
    <header id="main-header">
        <a id="main-nav-monogram" href="/"><?= svg('assets/images/logos/monogram.svg') ?></a>
        <a id="main-logo" href="/" title="Aller à l'accueil">
            <img src="<?= url('assets/images/logos/00.Logo_Flaash_no-baseline.svg') ?>"
                alt="FLAASH">
        </a>
        <nav id="main-nav">
            <a href="<?= page('numeros')->url() ?>"><?= page('numeros')->title() ?></a>
            <a href="<?= page('articles')->url() ?>"><?= page('articles')->title() ?></a>
            <a href="<?= page('newsletter')->url() ?>">Newsletter</a>
            <a href="<?= page('boutique')->link() ?>" target="_blank">Boutique</a>
        </nav>
        <button id="mobile-nav-btn">
            <span class="mobile-nav-btn__line mobile-nav-btn__line--top"></span>
            <span class="mobile-nav-btn__line mobile-nav-btn__line--middle"></span>
            <span class="mobile-nav-btn__line mobile-nav-btn__line--bottom"></span>
        </button>
        <div id="main-nav--mobile">

            <nav>
                <ul>
                    <li>
                        <a href="<?= page('numeros')->url() ?>"><?= page('numeros')->title() ?></a>
                    </li>
                    <li>
                        <a href="<?= page('articles')->url() ?>"><?= page('articles')->title() ?></a>
                    </li>
                    <li>
                        <a href="<?= page('newsletter')->url() ?>">Newsletter</a>
                    </li>
                    <li>
                        <a href="<?= page('boutique')->link() ?>" target="_blank">Boutique</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
    <div id="logo-caption">
        <h2>
            <span id="logo-caption__left" class="logo-caption__part">La<br>et</span>
            <span id="logo-caption__middle" class="logo-caption__part">revue<br>technique</span>
            <span id="logo-caption__right" class="logo-caption__part">culturelle<br>d'anticipation</span>
        </h2>
    </div>
    <main>