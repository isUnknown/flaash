<?php

function convertDateToFrench($date) {
    $dateArray = explode('/', $date);

    if (count($dateArray) != 3) {
        return "Date invalide";
    }

    $day = $dateArray[0];
    $month = $dateArray[1];
    $year = $dateArray[2];

    $monthsFrench = [
        1 => "janvier",
        2 => "février",
        3 => "mars",
        4 => "avril",
        5 => "mai",
        6 => "juin",
        7 => "juillet",
        8 => "août",
        9 => "septembre",
        10 => "octobre",
        11 => "novembre",
        12 => "décembre"
    ];

    $formattedDate = $day . " " . $monthsFrench[(int)$month] . " " . $year;

    return $formattedDate;
}
