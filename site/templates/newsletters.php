<?php snippet('header') ?>
<section class="grid">
  <div class="repository-presentation" style="--span: 7;">
    <h1><?= $page->title() ?></h1>
    <?= $page->presentation() ?>
    <form action="">
      <input placeholder="Email" type="email" name="email" autocomplete="email" id="email">
      <button type="submit">S'inscrire</button>
    </form>
  </div>
</section>
<section class="grid">
  <?php foreach($page->children() as $newsletter): ?>
    <div style="--span: 3;">
    <?php snippet('representative--article', ['article' => $newsletter]) ?>
    </div>
  <?php endforeach ?>
</section>
<?php snippet('footer') ?>