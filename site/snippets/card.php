<?php 
  $link = $link ?? null;
  $target = $target ?? null;
?>

<div class="card">
    <h4 class="card__title"><?= $title ?></h4>
    <?php if (isset($image)): ?>

      <?php if ($link): ?>
          <a href="<?= $link ?>" title="Aller à <?= $link ?>" <?= e(!str_contains($link, $site->url()), 'target="_blank"') ?>>
      <?php endif ?>

      <?php snippet('picture', ['file' => $image]) ?>

      <?php if ($link): ?>
          </a>
      <?php endif ?>
    
      <?php endif ?>
    <?php foreach($buttons as $button): ?>
      <?php if (strlen($button['link']) > 0): ?>
        <a class="button" href="<?= $button['link'] ?>" <?= e(!str_contains($button['link'], $site->url()), 'target="_blank"') ?>><?= $button['text'] ?></a>
      <?php endif ?>
    <?php endforeach ?>
</div>