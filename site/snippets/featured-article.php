<div class="featured-article" style="--span:9">
  <a href="<?= $article->url() ?>" title="Voir l'article">
    <?php snippet('picture', ['file' => $article->cover()->toFile()]) ?>
  </a>
  <div id="featured-article__content">
    <a href="<?= $article->url() ?>" class="no-line" title="Voir l'article">
      <h2 id="featured-article__title"><?= $article->title() ?></h2>
    </a>
    <p id="featured-article__infos" class="article__infos">
      par <?php snippet('contributor-link', ['contributorField' => $article->contributor()]) ?>, 
      <?php if ($article->parent()->template() == "issue"): ?>  
        à retrouver dans le
        <a href="<?= $article->parent()->url() ?>" title="Voir le n°<?= $article->parent()->number() ?>">
          n°<?= $article->parent()->number() ?>
          —
          <?= $article->parent()->period() ?>
          —
          <?= $article->parent()->title() ?>
        </a>
      <?php else: ?>
        <?= page("articles")->unlistedLabel() ?>
      <?php endif; ?>
    </p>
    <?php snippet('thematics', ['thematics' => $article->thematics()->split()]) ?>
    <p id="featured-article__chapo"><?= $article->chapo() ?></p>
    <a href="<?= $article->url() ?>" title="Lire la suite" class="read-more">Lire
      la suite</a>
  </div>
</div>