<?php snippet('header') ?>
<div 
  id="interactive-wrapper"
  x-data="{
    activeFilter: undefined,
    initialFilter: '<?= get('thematic') ?>',
    init() {
      if (!this.initialFilter) return
      
      this.activeFilter = this.initialFilter
    },
    toggleFilter(thematic) {
      if (this.activeFilter === thematic) {
        this.activeFilter = ''
      } else {
        this.activeFilter = thematic
      }
    }
  }"
>
  <section class="grid">
    <div class="repository-presentation" style="--span: 7;">
      <h1><?= $page->title() ?></h1>
      <?= $page->presentation() ?>
    </div>
    <ul 
      id="featured-article__thematics" 
      class="thematics" 
      style="--span: 12;"
    >
        <?php foreach($page->thematics()->split() as $thematic): ?>
        <li 
          class="thematic"
          x-data="{
            get isActive() {
              return this.activeFilter === '<?= $thematic ?>';
            }
          }"
          :class="isActive ? 'active' : ''"
        >
            <button @click="toggleFilter('<?= $thematic ?>')"><?= $thematic ?></button>
        </li>
        <?php endforeach ?>
    </ul>
  </section>

  <section id="results">
    <ul>
      <!--========== ISSUES ==========-->
      <li class="results__category">
        <div class="results__banner">
          <h2>Numéros</h2>
        </div>
        <ul class="results__list grid">
          <?php foreach(page('numeros')->children() as $issue): ?>
          <li 
            style="--span: 3;"
            x-data='{
              thematics: <?= Data::encode($issue->thematics()->split(), 'json') ?>,
              isDisplayed() {
                return !this.activeFilter || this.thematics.some(thematic => this.activeFilter === thematic)
              }
            }'
            x-show="isDisplayed()"
          >
            <?php snippet('representative--issue', ['issue' => $issue]) ?>
          </li>
          <?php endforeach ?>
        </ul>  
      </li>
      
      <!--========== ARTICLES ==========-->
      <li class="results__category">
        <div class="results__banner">
          <h2>Articles</h2>
        </div>
        <ul class="results__list grid">
          <?php foreach($kirby->collection('articles') as $article): ?>
            <li 
              style="--span: 3;"
              x-data='{
                thematics: <?= Data::encode($article->thematics()->split(), 'json') ?>,
                isDisplayed() {
                  return !this.activeFilter || this.thematics.some(thematic => this.activeFilter === thematic)
                }
              }'
              x-show="isDisplayed()"
            >
              <?php snippet('representative--article', ['article' => $article]) ?>
            </li>
          <?php endforeach ?>
        </ul>
      </li>
      
      <!--========== Newsletter ==========-->
      <li class="results__category">
        <div class="results__banner">
          <h2>Newsletter</h2>
        </div>
        <ul class="results__list grid">
          <?php foreach(page('newsletter')->children() as $newsletter): ?>
          <li 
            style="--span: 3;"
            x-data='{
              thematics: <?= Data::encode($newsletter->thematics()->split(), 'json') ?>,
              isDisplayed() {
                return !this.activeFilter || this.thematics.some(thematic => this.activeFilter === thematic)
              }
            }'
            x-show="isDisplayed()"
          >
            <?php snippet('representative--article', ['article' => $newsletter]) ?>
          </li>
          <?php endforeach ?>
        </ul>  
      </li>
    </ul>
  </section>
</div>
<?php snippet('footer') ?>