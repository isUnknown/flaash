<?php snippet('header') ?>
<h1><?= $page->title() ?></h1>
  <?php snippet('picture', ['file' => $page->cover()->toFile()]) ?>
<article>
  
  <?= $page->body()->toBlocks() ?>

</article>
<?php snippet('footer') ?>