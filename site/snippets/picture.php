<?php if ($file): ?>
    <?php
    
    $sizes = '(min-width: 1200px) 25vw, (min-width: 900px) 33vw, (min-width: 600px) 50vw, 100vw';
    $alt = method_exists($file, 'alt') ? $file->alt() : 'Fragment de carte de la France';
    $crop = $crop ?? false;
    
    $webPSrcset = $crop === 'banner' ? $file->crop(3200, 1600)->srcset('webp') : $file->srcset('webp');
    $srcset = $crop === 'banner' ? $file->crop(3200, 1600)->srcset() : $file->srcset();
    $src = $crop === 'banner' ? $file->crop(3200, 1600)->url() : $file->url();
    $width = $crop === 'banner' ? $file->crop(3200, 1600)->width() : $file->resize(1800)->width();
    $height = $crop === 'banner' ? $file->crop(3200, 1600)->height() : $file->resize(1800)->height();
    
    ?>
    
    
    
    <picture>
        
        <source srcset="<?= $webPSrcset ?>"
            sizes="<?= $sizes ?>" type="image/webp">
        <img 
            src="<?= $src ?>"
            srcset="<?= $srcset ?>"
            sizes="<?= $sizes ?>"
            width="<?= $width ?>"
            height="<?= $height ?>"
            alt="<?= $alt?>"
            loading="lazy"
        >
        <?= svg('assets/images/loader.svg') ?>
    </picture>
<?php endif ?>