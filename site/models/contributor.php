<?php

class ContributorPage extends Page {
    public function issues() {
        $issues = page('numeros')->children();
        $contributorIssues = [];
        
        foreach ($issues as $issue) {
            // Filtre les articles où le contributeur contient la page actuelle
            $contributorArticles = $issue->children()->filter(function ($article) {
                return $article->contributor()->isNotEmpty() && 
                       $article->contributor()->toPages()->has($this);
            });
            
            if ($contributorArticles->count() === 0) continue;

            $contributorIssues[] = [
                'title' => $issue->title()->value(),
                'number' => $issue->number(),
                'url' => $issue->url(),
                'articles' => $contributorArticles
            ];
        }

        $unlistedArticles = page("articles")->children()->filter(function ($unlistedArticle) {
            return $unlistedArticle->contributor()->isNotEmpty() && 
                    $unlistedArticle->contributor()->toPages()->has($this);
        });

        if ($unlistedArticles->count() !== 0) {
            $contributorIssues[] = [
                'url' => page('articles')->url(),
                'articles' => $unlistedArticles
            ];
        }

        return $contributorIssues;
    }
}
