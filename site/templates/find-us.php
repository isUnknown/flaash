<?php snippet('header') ?>
<section class="grid">
  <div class="repository-presentation" style="--span: 7;">
    <h1><?= $page->title() ?></h1>
    <?= $page->presentation() ?>
  </div>
</section>
<section class="grid">
  <div style="--span: 9;">
    <a id="map" href="https://web2store.mlp.fr/produit.aspx?edi_code=lT4Hrvj3Q%2bc%3d&tit_code=iTK83lfTxcA%3d" target="_blank" title="Voir les points de vente">
      <?php snippet('picture', ['file' => asset('assets/images/map.png')]) ?>
    </a>
  </div>
  <div style="--span:3">
      <?php snippet('card', [
          'title'   => "S'abonner",
          'image' => page('boutique')->cardCover()->toFile() ?? null,
          'link' => page('boutique')->link(),
          'target' => 'external',
          'buttons' => [
              [
                  'text' => 'Abonnement papier',
                  'link' => page('boutique')->printSubmissionLink()
              ],
              [
                  'text' => 'Abonnement digital',
                  'link' => page('boutique')->digitalSubmissionLink()
              ]
          ]
      ]) ?>
    </div>
  </div>
</section>
<section id="issues-list">
    <h2 class="h1">Agenda des sorties</h2>
    <?= $page->agenda() ?>
</section>
<?php snippet('footer') ?>