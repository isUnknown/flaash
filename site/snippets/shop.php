<section id="shop">
  <a href="<?= page('boutique')->link() ?>" class="section-title--center" target="_blank">
    <h3>Boutique</h3>
  </a>
  <div id="products">
    <?php foreach(page('boutique')->children() as $product): ?>
    <div class="product">
      <a href="<?= $product->link() ?>" target="_blank">
        <?php snippet('picture', ['file' => $product->cover()->toFile()]) ?>
      </a>
      <div class="product__description">
        <a class="no-line" href="<?= $product->link() ?>" target="_blank">
          <?= $product->description()->kt() ?>
        </a>
      </div>
      <a href="<?= $product->link()->toUrl() ?>" class="button" target="_blank">
        <?= e($product->buttonText()->isNotEmpty(), $product->buttonText(), 'commander') ?>
      </a>
    </div>
    <?php endforeach ?>
  </div>
</section>